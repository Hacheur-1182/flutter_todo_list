import 'package:flutter/material.dart';

class ListTitle extends StatefulWidget {
  const ListTitle({key}) : super(key: key);

  @override
  _ListTitleState createState() => _ListTitleState();
}

class _ListTitleState extends State<ListTitle> {
  final List<Color> colors = <Color>[
    const Color(0xffFEE99C),
    const Color(0xff9CD4FF),
    const Color(0xff9CFFB2),
    const Color(0xff161618),
  ];
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(
        height: 60.0,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Color(0xff9CD4FF),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(
                100.0,
              ),
              topLeft: Radius.circular(
                20.0,
              )),
        ),
        child: const Text(
          "children",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    ]);
  }
}
