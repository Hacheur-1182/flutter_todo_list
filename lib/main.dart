import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/UI/addTaskScreen.dart';
import 'package:todoey/UI/taskScreen.dart';
import 'package:todoey/provider/data.dart';

void main() {
  runApp(
    const TodoEy(),
  );
}

class TodoEy extends StatelessWidget {
  const TodoEy({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Data>(
      create: (context) => Data(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData.dark().copyWith(
          scaffoldBackgroundColor: Colors.purple,
        ),
        home: Scaffold(
          floatingActionButton: Builder(
            builder: (context) => CircleAvatar(
              backgroundColor: Colors.purple,
              maxRadius: 30.0,
              child: TextButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    context: context,
                    builder: (context) => const AddTaskScreen(),
                  );
                },
                child: const Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 30.0,
                ),
              ),
            ),
          ),
          body: TaskScreen(),
        ),
      ),
    );
  }
}