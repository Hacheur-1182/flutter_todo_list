import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:todoey/components/taskTile.dart';
import '../provider/data.dart';

class TaskList extends StatelessWidget {
  const TaskList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<Data>(
      builder: (BuildContext context, data, Widget child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final task = data.task[index];
            return TaskTile(
              task: task.name,
              checked: task.checked,
              isChecked: (value) {
                data.updateTask(task);
              },
              deleted: (BuildContext context){
                data.deleteTask(task);
              },
              onTapToCheck: (){
                data.updateTask(task);
              },
            );
          },
          itemCount: data.taskCount,
        );
      },
    );
  }
}
