import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class TaskTile extends StatelessWidget {
  const TaskTile({key, this.task, this.checked, this.isChecked, this.deleted, this.onTapToCheck})
      : super(key: key);
  final String task;
  final bool checked;
  final Function isChecked;
  final Function deleted;
  final Function onTapToCheck;
  @override
  Widget build(BuildContext context) {
    return Slidable(
      startActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: deleted,
            backgroundColor: Colors.red,
            icon: Icons.delete,
            foregroundColor: Colors.white,
            label: 'Delete',
          ),
        ],
      ),
      child: ListTile(
        onTap: onTapToCheck,
        title: Text(
          task,
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
            decoration: checked ? TextDecoration.lineThrough : null,
          ),
        ),
        trailing: Checkbox(
          checkColor: Colors.white,
          focusColor: Colors.purple,
          activeColor: Colors.purple,
          fillColor: MaterialStateProperty.all(
            Colors.purple,
          ),
          value: checked,
          onChanged: isChecked,
        ),
      ),
    );
  }
}
