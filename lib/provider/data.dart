import 'dart:collection';

import 'package:flutter/cupertino.dart';

import '../models/tasks.dart';

class Data extends ChangeNotifier{
   final List<Task> _task = [
    Task(name: "Press button to add Task"),
    Task(name: "Check task complete ->"),
    Task(name: "Swipe to right to delete task"),
  ];
   UnmodifiableListView<Task> get task{
     return UnmodifiableListView(_task);
   }
  int get taskCount{
    return _task.length;
  }
  void onChange(String newText){
    _task.add(Task(name: newText));
    notifyListeners();
  }
  void updateTask(Task task){
    task.isChecked();
    notifyListeners();
  }
  void deleteTask(Task task){
    _task.remove(task);
    notifyListeners();
  }
}